import os
import sys
from datetime import datetime
from kivy.metrics import dp
from kivy.uix.widget import Widget
from kivy.factory import Factory
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.clock import Clock
from kivy.core.window import Window
from kivy.lang import Builder
from kivy.uix.image import Image
from kivy.uix.modalview import ModalView
from kivy.utils import get_hex_from_color
from kivy.uix.screenmanager import ScreenManager, Screen
import pickle as pickle
from kivymd.utils.cropimage import crop_image
from kivymd.fanscreenmanager import MDFanScreen
from kivymd.popupscreen import MDPopupScreen
from kivymd.button import MDIconButton
from kivymd.list import ILeftBody, ILeftBodyTouch, IRightBodyTouch
from kivymd.material_resources import DEVICE_TYPE
from kivymd.selectioncontrols import MDCheckbox
from kivymd.theming import ThemeManager
from kivymd.card import MDCard
from kivymd.list import OneLineListItem
from kivymd.list import MDList
from kivymd.button import MDFloatingActionButton
from inv_math import Inventarisation, Invent_MNPP, Density
from decimal import Decimal, getcontext, ROUND_HALF_UP


def normal_round(x, digits, precision=12):
    round_context = getcontext()
    round_context.rounding = ROUND_HALF_UP
    tmp = round(Decimal(x), precision)
    return float(tmp.__round__(digits))


class AO_cls:
    def __init__(self, name, superclass=None):
        self.child = []
        self.name = name
        self.super = superclass
        self.add_window = 'Add_MNPP_window'
        self.tree_end = False

    def add_child_mem(self, mem):
        name_list = [str(x) for x in self.child]
        if not (str(mem) in name_list):
            self.child.append(mem)
            return(True)
        else:
            return(False)

    def __getitem__(self, i):
        return self.child[i]

    def __str__(self):
        return self.name


class MNPP_cls(AO_cls):
    def __init__(self, name, superclass):
        super().__init__(name, superclass)
        self.super = superclass
        self.add_window = 'Uch_Enter_window'
        self.invent_list = []
        self.tree_end = True


class UCH_cls(MNPP_cls):

    def __init__(self, name, superclass, Length, Value, d_vn):
        super().__init__(name, superclass)
        self.add_window = ''
        self.Length = Length
        self.Value = Value
        self.d_vn = d_vn
        self.add_window = ''
        self.invent_history = []

    def make_invent(self, D_n, D_k, T_n, T_k, P_n, P_k):
        Invent_obj = Invent_MNPP(D_n=D_n, D_k=D_k, T_n=T_n,
                                 T_k=T_k, P_n=P_n, P_k=P_k, V_grad=self.Value,
                                 L_uch=self.Length, d_sr=self.d_vn)
        time = datetime.now()
        self.invent_history.append([time, Invent_obj])


# Начало всего
AO = AO_cls('АО "Транснефть - Дружба"')
try:
    with open('Data_MNPP.dat', 'rb') as f:
        AO = pickle.load(f)
except:
    with open('Data_MNPP.dat', 'wb') as f:
        pickle.dump(AO, f)


class Invent_count(Screen):

    def __init__(self, **kw):
        super(Invent_count, self).__init__(**kw)
        cur_path = os.path.dirname(__file__)
        new_path = os.path.join(cur_path, 'Invent_Menu.kv')
        with open(new_path, encoding='utf-8') as f:
            temp_f = f.read()
        Builder.load_string(temp_f)
        self.Obj_cont = None


    def back_button(self):
        self.manager.current = 'Select_menu'

    def back_button_window(self):
        Invent._running_app.root.ids.toolbar.left_action_items = [
            ['arrow-left', lambda x: self.back_button()]]

    def default_state(self, val_arr):
        for x in val_arr:
            x.text = ''
        Invent._running_app.root.ids.toolbar.left_action_items = Invent._running_app.action_bar_temp
        self.ids.Den_switch_n._active = False
        self.ids.Den_switch_k._active = False

    def on_enter(self):
        self.back_button_window()

    def make_invent(self):
        D_n = normal_round(x=self.ids.D_n.text, digits=2)
        D_k = normal_round(x=self.ids.D_k.text, digits=2)
        P_n = normal_round(x=self.ids.P_n.text, digits=2)
        P_k = normal_round(x=self.ids.P_k.text, digits=2)
        T_n = normal_round(x=self.ids.T_n.text, digits=1)
        T_k = normal_round(x=self.ids.T_k.text, digits=1)
        switch_d_n = self.ids.Den_switch_n._active
        switch_d_k = self.ids.Den_switch_k._active
        # расчитываем плотность с включенным переключателем
        if switch_d_n:
            D_n = Density(D_n, T_n, P_n).D_to_T()
        if switch_d_k:
            D_k = Density(D_k, T_k, P_k).D_to_T()
        val_arr = [self.ids.D_n,
                   self.ids.D_k, self.ids.P_n, self.ids.P_k, self.ids.T_n, self.ids.T_k]
        boo = [0 for x in val_arr if x.text == '']
        if not boo:
            self.Obj_cont.make_invent(D_n=D_n, D_k=D_k, T_n=T_n,
                                      T_k=T_k, P_n=P_n, P_k=P_k)
            with open('Data_MNPP.dat', 'wb') as f:
                pickle.dump(AO, f)
            self.manager.current = 'Select_menu'
            self.default_state(val_arr)
        return


class MNNP_Name_Enter(Screen):
    def __init__(self, **kw):
        super(MNNP_Name_Enter, self).__init__(**kw)
        cur_path = os.path.dirname(__file__)
        new_path = os.path.join(cur_path, 'Add_Window.kv')
        with open(new_path, encoding='utf-8') as f:
            temp_f = f.read()
        Builder.load_string(temp_f)
        self.action_bar_temp = None
        self.superclass = None

    def default_state(self):
        self.ids.MNPP_name.text = ''
        Invent._running_app.root.ids.toolbar.left_action_items = Invent._running_app.action_bar_temp
        self.action_bar_temp = 0
        self.manager.current = 'Select_menu'

    def back_button(self):
        self.manager.current = 'Select_menu'

    def back_button_window(self):
        self.action_bar_temp = Invent._running_app.root.ids.toolbar.left_action_items
        Invent._running_app.root.ids.toolbar.left_action_items = [
            ['arrow-left', lambda x: self.back_button()]]

    def on_enter(self):
        self.ids.MNPP_name.focused = True
        self.ids.MNPP_name.helper_text_mode = "on_error"
        self.back_button_window()

    def add_mnpp(self):
        self.ids.MNPP_name.focused = True
        text = self.ids.MNPP_name.text
        if text:
            MNPP = MNPP_cls(name=text, superclass=self.superclass)
            if not self.superclass.add_child_mem(MNPP):
                self.ids.MNPP_name.helper_text_mode = "persistent"
                self.ids.MNPP_name.helper_text = 'Такой МНПП уже есть!!!'
                return
            else:
                self.superclass.add_child_mem(MNPP)
                with open('Data_MNPP.dat', 'wb') as f:
                    pickle.dump(AO, f)
                self.default_state()
        return


class BlankWindow(Screen):

    def __init__(self, **kw):
        cur_path = os.path.dirname(__file__)
        new_path = os.path.join(cur_path, 'BlankWindow.kv')
        with open(new_path, encoding='utf-8') as f:
            temp_f = f.read()
        Builder.load_string(temp_f)
        super(BlankWindow, self).__init__(**kw)


class Select_Menu(Screen):
    def __init__(self, **kw):
        super(Select_Menu, self).__init__(**kw)
        self.ID_menu = 0
        cur_path = os.path.dirname(__file__)
        new_path = os.path.join(cur_path, 'Select_Menu.kv')
        with open(new_path, encoding='utf-8') as f:
            temp_f = f.read()
        Builder.load_string(temp_f)
        self.action_bar_temp = None
        self.Select_object_menu = True

    def back_button(self):
        self.ID_menu = self.itr_obj.super
        self.manager.current = 'Select_menu'
        self.on_enter()


    def select_menu_form(self, itr_obj, button_plus=True):

        def make_back_button():
            Invent._running_app.root.ids.toolbar.left_action_items = [
                    ['arrow-left', lambda x: self.back_button()]]

        def default_state():
            Invent._running_app.root.ids.toolbar.left_action_items = Invent._running_app.action_bar_temp

        def callback(instance):
            name = instance.text
            if itr_obj.add_window:
                if itr_obj:
                    for name_itr_object in itr_obj:
                        if str(name_itr_object) == name:
                            self.ID_menu = name_itr_object
                            self.ids.ml.clear_widgets()
                            self.on_enter()
                            break
            return
        itr_obj_names = [str(x) for x in itr_obj]
       # кнопка назад в меню
        if itr_obj.super:
            make_back_button()
        else: 
            default_state()
        
        self.ids.ml.clear_widgets()
        self.ids.lay.clear_widgets()
        # Рисуем список объектов
        for f in itr_obj_names:
            fd = f
            if not itr_obj.tree_end:  # отключаем нажатие кнопок в конце дерева
                btn = OneLineListItem(text=fd, on_release=callback)
            else:
                if button_plus:
                    btn = OneLineListItem(text=fd)
                else:
                    def invent_button(instance):
                        self.manager.current = "Invent_count"
                        name = instance.text
                        if itr_obj.add_window:
                            if itr_obj:
                                for name_itr_object in itr_obj:
                                    if str(name_itr_object) == name:
                                        self.manager.get_screen(
                                            name='Invent_count').Obj_cont = name_itr_object
                                        break
                        return
                    btn = OneLineListItem(
                        text=fd, on_release=invent_button)
            self.ids.ml.add_widget(btn)

        """Создаем кнопку плюc"""
        if button_plus:
            if itr_obj.add_window:
                def on_click(instance):
                    self.manager.current = itr_obj.add_window
                    self.manager.get_screen(
                        itr_obj.add_window).superclass = itr_obj
                btnplus = MDFloatingActionButton(
                    icon='plus', opposite_colors=True, elevation_normal=8, on_release=on_click)
                self.ids.lay.add_widget(btnplus)
        else:
            self.ids.lay.clear_widgets()

    def on_enter(self):
        self.itr_obj = self.ID_menu
        self.select_menu_form(
            self.itr_obj, button_plus=self.Select_object_menu)

    def on_leave(self):
        self.ids.ml.clear_widgets()
        self.ids.lay.clear_widgets()


class Uch_Enter(Screen):
    def __init__(self, **kw):
        super(Uch_Enter, self).__init__(**kw)
        "считываем конфиг сделать функцией!!!!!"
        cur_path = os.path.dirname(__file__)
        new_path = os.path.join(cur_path, 'Uch_Enter.kv')
        with open(new_path, encoding='utf-8') as f:
            temp_f = f.read()
        Builder.load_string(temp_f)
        self.superclass = None
    
    def back_button(self):
        self.manager.current = 'Select_menu'

    def back_button_window(self):
        Invent._running_app.root.ids.toolbar.left_action_items = [
            ['arrow-left', lambda x: self.back_button()]]
        
    def default_state(self,val_arr):
        self.manager.current = 'Select_menu'
        Invent._running_app.root.ids.toolbar.left_action_items = Invent._running_app.action_bar_temp
        self.action_bar_temp = 0
        for x in val_arr:
            x = ''
        
    def on_enter(self):
        self.ids.MNPP_name.hint_text = "Введите название участка МНПП"
        self.ids.MNPP_name.focused = True
        self.ids.MNPP_name.helper_text = ''
        self.back_button_window()
        self.ids.MNPP_name.helper_text_mode = "on_error"

    def add_uch(self):
        self.add_uch()
        self.ids.MNPP_name.focused = True
        text = self.ids.MNPP_name.text
        Length = normal_round(x=self.ids.Value.text, digits=3)
        Value = normal_round(x=self.ids.Value.text, digits=3)
        d_vn = normal_round(x=self.ids.d_vn.text, digits=2)
        val_arr = [self.ids.MNPP_name.text,
                   self.ids.Length.text, self.ids.Value.text, self.ids.d_vn.text]
        uch = UCH_cls(name=text, superclass=self.superclass,
                      Length=Length, Value=Value, d_vn=d_vn)
        boo = [0 for x in val_arr if x == '']
        if not boo:
            if not self.superclass.add_child_mem(uch):
                self.ids.MNPP_name.helper_text_mode = "persistent"
                self.ids.MNPP_name.helper_text = 'Такой участок МНПП уже есть!!!'
                return
            else:
                self.superclass.add_child_mem(uch)
                with open('Data_MNPP.dat', 'wb') as f:
                    pickle.dump(AO, f)
                self.default_state(val_arr)
        return


class MenuScreen(Screen):
    def __init__(self, **kw):
        super(MenuScreen, self).__init__(**kw)
        self.action_bar_temp = 0

    def on_enter(self):
        if self.action_bar_temp:
            Invent._running_app.root.ids.toolbar.left_action_items = self.action_bar_temp


class Invent(App):
    theme_cls = ThemeManager()
    theme_cls.primary_palette = 'Blue'
    title = "Инвентаризация"

    def __init__(self, **kwargs):
        super(Invent, self).__init__(**kwargs)
        cur_path = os.path.dirname(__file__)
        new_path = os.path.join(cur_path, 'main_screen.kv')
        with open(new_path, encoding='utf-8') as f:
            temp_f = f.read()
        Builder.load_string(temp_f)
        self.main_layout = Factory.MainLayout()
        self.action_bar_temp = None

    def build(self):
        #Сохраняем кнопку меню
        return self.main_layout

    def select_menu_start(self):
        self.root.ids.scr_mngr.get_screen(name='Select_menu').ID_menu = AO
        self.root.ids.scr_mngr.get_screen(
            name='Select_menu').Select_object_menu = True
        self.root.ids.scr_mngr.current = 'main'
        self.root.ids.scr_mngr.current = 'Select_menu'
        self.action_bar_temp = Invent._running_app.root.ids.toolbar.left_action_items
    
    def select_invent_menu(self):
        self.root.ids.scr_mngr.get_screen(
            name='Select_menu').ID_menu = AO
        self.root.ids.scr_mngr.get_screen(
            name='Select_menu').Select_object_menu = False
        self.root.ids.scr_mngr.current = 'main'
        self.root.ids.scr_mngr.current = 'Select_menu'
        self.action_bar_temp = Invent._running_app.root.ids.toolbar.left_action_items

if __name__ == '__main__':
    Invent().run()
