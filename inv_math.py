﻿# -*- coding: utf-8 -*-
"""
Created on Mon Jan 28 10:32:12 2019

@author: KovrigaAA
"""
from decimal import Decimal, getcontext, ROUND_HALF_UP
import math


class Inventarisation:


        def normal_round(self, x, digits, precision=12):
            round_context = getcontext()
            round_context.rounding = ROUND_HALF_UP
            tmp = round(Decimal(x), precision)
            return float(tmp.__round__(digits))
            
            
        def y_T_cp_f(self, D_i_i, T_cp):
            y_T_cp = self.normal_round(0.001*math.exp(-1.62080+0.00021592*T_cp+(0.87096*(10**6)/((D_i_i)**2))+(4.2092*T_cp*(10**3)/((D_i_i)**2))), 6)
            return y_T_cp

        def b15_f(self, D_i_i):
                if D_i_i >= 611.2 and D_i_i < 770.9:
                    K0 = 346.4228
                    K1 = 0.43884
                    K2 = 0
                elif D_i_i >= 770.9 and D_i_i < 788.0:
                    K0 = 2690.7440
                    K1 = 0
                    K2 = -0.0033762
                elif D_i_i >= 788.0 and D_i_i < 838.7:
                    K0 = 594.5418
                    K1 = 0
                    K2 = 0
                elif D_i_i >= 838.7 and D_i_i < 1163.9:
                    K0 = 186.9696
                    K1 = 0.4862
                    K2 = 0
                b15 = (K0+K1*D_i_i)/(D_i_i**2)+K2
                return b15

        def T_cp(self):
            self.T_cp = self.normal_round(((self.T_n+self.T_k)*0.5), 1)

        def P_cp(self):
            self.P_cp = self.normal_round(((self.P_n+self.P_k)*0.5), 2)

        def D_cp(self, D_n, D_k):
            D_cp = self.normal_round((D_n+D_k)*0.5, 2)
            return D_cp


        

class Invent_MNPP(Inventarisation):
    def __init__(self, D_n, D_k, T_n, T_k, P_n, P_k, V_grad, L_uch, d_sr):
        self.D_k = D_k
        self.D_n = D_n
        self.T_n = T_n
        self.T_k = T_k
        self.P_n = P_n
        self.P_k = P_k
        self.V_grad = V_grad
        self.L_uch = L_uch
        self.d_sr = d_sr
        self.T_cp()
        self.P_cp()
        self.Den_n = Density(self.D_n, self.T_n, self.P_n)
        self.Den_k = Density(self.D_k, self.T_k, self.P_k)
        self.Kt()
        self.Kp()
        self.Kper()
        self.V_prived()
        self.M_uch()

    def Kt(self):
        Tcp = self.T_cp
        a = 1.2*10**(-5)
        self.Kt = self.normal_round((1+3*a*(Tcp-20)), 6)

    def Kp(self):
        E = 2.06*10**5
        pi = 3.1416
        L_uch = self.L_uch
        Pcp = self.P_cp
        Vgard = self.V_grad
        Duch = 1000*((4*Vgard/(pi*L_uch))**(1/2))
        d_uch = self.d_sr
        self.Kp = self.normal_round((1+(Pcp)/E*(Duch/d_uch)), 6)

    def Kper(self):
        Tcp = self.T_cp
        Pcp = self.P_cp
        D_cp_15 = self.D_cp(self.Den_n.D_15, self.Den_k.D_15)
        self.D_cp_15 = D_cp_15
        D_cp_20 = self.D_cp(self.Den_n.D_20, self.Den_k.D_20)
        self.D_cp_15 = D_cp_15
        self.D_cp_20 = D_cp_20
        K_t_per = self.normal_round(math.exp((self.b15_f(D_cp_15)*(Tcp-15))*(1+0.8*self.b15_f(D_cp_15)*(Tcp-15))), 6)
        K_p_per = self.normal_round((1-self.y_T_cp_f(D_cp_15, Tcp)*Pcp), 6)
        Kper = self.normal_round(((D_cp_20/D_cp_15)*K_t_per*K_p_per), 6)
        self.Kper = Kper

    def V_prived(self):
        V_grad = self.V_grad
        self.V_prived = self.normal_round((V_grad*self.Kt*self.Kp/self.Kper), 3)

    def M_uch(self):
        V_prived = self.V_prived
        D_cp_20 = self.D_cp(self.Den_n.D_20, self.Den_k.D_20)
        self.M_uch = self.normal_round(V_prived*D_cp_20*0.001, 3)


class Density(Inventarisation):
        """
        Класс для приведения плотности к стандартным условиям, расчет взят из МИ3275-2016 
        с коррректировкой формулы Е.13(убран знак - из экспоненты, знаменатель перенесен в множитель)
        D_i-плотность измеренная кг/м3
        T-температура, С
        P- давление, Мпа
        -----------результат------------------------
        D_15-плотность при 15С
        D_20-плотность при 20С
        """
        def __init__(self, D_i, T, P):
            self.D_i = D_i
            self.T_cp = T
            self.P_cp = P
            self.D_15 = 0
            self.D_20 = 0
            self.D_to_15()
            self.D_to_20()

        def D_to_15(self):
            T_cp = self.T_cp
            P_cp = self.P_cp
            D_i = self.D_i
            D_i_i = self.D_i
            D_i_old = self.D_i
            while True:
                y_T_cp = self.y_T_cp_f(D_i_i, T_cp)
                b15 = self.b15_f(D_i_i)
                D_i_i = (D_i*math.exp(b15*(T_cp-15)*(1+0.8*b15*(T_cp-15))))*(1-y_T_cp*P_cp)
                if math.fabs(D_i_old-D_i_i) > 0.01:
                    D_i_old = D_i_i
                else:
                    break
            self.D_15 = self.normal_round(D_i_i, 2)

        def D_to_20(self, D_15=0):
            k = 1
            if D_15 == 0:
                k = 0
                D_15 = self.D_15
            b15 = self.b15_f(D_15)
            D_20 = D_15*math.exp(-5*b15*(1+4*b15))
            D_20 = self.normal_round(D_20, 2)
            if k == 0:
                self.D_20 = D_20
            else:
                return(D_20)

        def D_to_15_Nuton(self):
            T = self.T_cp
            P = self.P_cp
            D_i = self.D_i
            D_i_i = self.D_i
            D_fact = self.D_i
            i = 0
            while True:
                y_T = self.y_T_cp_f(D_i_i, T)
                b15 = self.b15_f(D_i_i)
                D_fact = (D_i_i*math.exp(-b15*(T-15)*(1+0.8*b15*(T-15))))/(1-y_T*P)
                i = D_i-D_fact+i
                D_i_i = D_i+i
                if math.fabs(D_fact-D_i) < 0.00000001:
                    break
            self.D_15 = self.normal_round(D_i_i, 2)

        def D_to_T(self, Tpriv=20):
            T_cp = self.T_cp
            P_cp = self.P_cp
            Dens = Density(self.D_i, Tpriv, 0)
            D_T_15 = Dens.D_15
            D_i = D_T_15
            D_i_i = D_T_15
            D_i_old = D_T_15
            while True:
                y_T_cp = self.y_T_cp_f(D_i, T_cp)
                b15 = self.b15_f(D_i)
                D_i_i = (D_i*math.exp(-b15*(T_cp-15)*(1+0.8*b15*(T_cp-15))))/(1-y_T_cp*P_cp)
                if math.fabs(D_i_old-D_i_i) > 0.0001:
                    D_i_old = D_i_i
                else:
                    break
            D_T = self.normal_round(D_i_i, 2)
            return(D_T)


